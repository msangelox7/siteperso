# Changelog

## 26/07/2021 Update

- Ajout du premier build New World

## 25/07/2021 Update

- Création de la page dédiée à New World

## 17/06/2021 Update

- Remplissage JSON
- Affichage des logs
- Ajout d'un titre de page, par page

## 16/06/2021 Update

- Ajout de contenu pour les modules de BF5 et BF1
- Ajout de lien sur les cartes BF
- Modification de la menu bar

## 15/06/2021 Update

- Modification des guildes
- Ajout dans le carousel de Battlefield 2042
- Ajout d'une page de records pour KALI

## 24/05/2021 Update

- Ajout d'un changement de langue pour les pages de records et de boss checker

## 23/05/2021 Update

- Ajout d'une page de records pour la guilde NeoS
- Modification des guildes

## 15/04/2021 Update

- Fix d'un bug avec l'affichage mobile sur la page de raids
- Ajout d'un local storage pour la clé api
- Gestion des erreurs retournées par l'api Guild Wars 2

## 14/04/2021 Update

- Ajout de la page d'affichage des boss de raid
- Modification de certaines images des boss de raid
- Ajout d'un champ d'insertion pour entrer sa clé API et voir ses boss faits

## 13/04/2021 Update

- Ajout des pages de mods pour Skyrim et Fallout 4
- Modification des couleurs pour la dropbox du menu
- Ajout des fichiers json pour les mods

## 09/04/2021 Update

- Ajout d'une page de chargement

## 02/04/2021 Update

- Ajout du player twitch sur la page principale

## 01/04/2021 Update

- Modification du nom de domaine : [`yuusuke.fr`](https://yuusuke.fr)
- Ajout d'un formulaire de contact sur la page d'affichage des projets

## 30/03/2021 Update

- Affichage des différents projets sur la page Projets
- Création des pages de guildes ainsi que de leur affichage

## 29/03/2021 Update

- Création du router React
- Création de la page Projet