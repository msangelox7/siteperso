import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Footer from './Components/Footer';
import Games from './Components/Games';
import Module from './Components/Module';
import Project from './Components/Project';
import {displayGuild} from './Components/Guild';
import TwitchPlayer from './Components/TwitchPlayer';
import Loading from './Components/Loading';
import Mods from './Components/Mods';
import Raids from './Components/Raids';
import Record from './Components/Record';
import Log from './Components/Logs';
import Tea from './Components/Tea';

export default class App extends Component {
  constructor() {
    super();
    this.state = { isLoading: true };
  }

  componentDidMount(){
      this.setState({
        isLoading: false
      })
    
  }


  render(){
  if (this.state.isLoading){
    return (
      <div className="loadingPage">
        <div className="loadingBox">
          <Loading />
        </div>
      </div>
    )
  }
  return (
    <div className="App">
    <Router>
      <Navbar bg="dark" variant="dark" fixed="top" expand="sm">
        <Navbar.Brand>
          <Link to="/">
            <div className="logo d-inline-block align-top" />
            Yuusuke
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <NavDropdown title="Guild Wars 2" id="basic-nav-dropdown">
              <NavDropdown.Item href="/guildwars2">
                Boss Checker
              </NavDropdown.Item>
              <NavDropdown.Item href="/guilds">
                Guildes
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Logs" id="basic-nav-dropdown">
              <NavDropdown.Item href="/KALI/logs">
                KALI
              </NavDropdown.Item>
              <NavDropdown.Item href="/WIPE/logs">
                WIPE
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Records" id="basic-nav-dropdown">
              <NavDropdown.Item href="/records/KALI">
                KALI
              </NavDropdown.Item>
              <NavDropdown.Item href="/records/WIPE">
                WIPE
              </NavDropdown.Item>
            </NavDropdown>
            <NavDropdown title="Mods List" id="basic-nav-dropdown">
              <NavDropdown.Item href="/skyrim">
                Skyrim
              </NavDropdown.Item>
              <NavDropdown.Item href="/fallout4">
                Fallout4
              </NavDropdown.Item>
            </NavDropdown>
            <Nav.Link>
             <Link to="/projects">Projets</Link>
            </Nav.Link>
            <Nav.Link href="/projects#contact">Contact</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Navbar>

      <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route path="/projects">
            <Projects />
          </Route>
          
          <Route exact path="/guilds">
            <Guilds />
          </Route>
          <Route path="/guilds/Kalimero">
            <Guilds guild="kalimero" />
          </Route>
          <Route path="/guilds/The Rainbow Triad">
            <Guilds guild="therainbowtriad" />
          </Route>
          <Route path="/guilds/Pink Sparkle Rainbow Unicorns">
            <Guilds guild="pinksparklerainbowunicorns" />
          </Route>
          <Route path="/guilds/Raid I Like To Farm">
            <Guilds guild="raidiliketofarm" />
          </Route>
          <Route path="/guilds/Neo Synergy">
            <Guilds guild="neosynergy" />
          </Route>

          <Route path="/skyrim">
            <Skyrim />
          </Route>
          <Route path="/fallout4">
            <Fallout4 />
          </Route>

          <Route path="/guildwars2">
            <Raid />
          </Route>

          
          <Route exact path="/records/KALI">
            <Records team="KALI" />
          </Route>
          <Route exact path="/records/WIPE">
            <Records team="WIPE" />
          </Route>

          <Route path="/KALI/logs">
            <Logs team="KALI" />
          </Route>
          <Route path="/WIPE/logs">
            <Logs team="WIPE" />
          </Route>

          <Route path="/autres/thé">
            <Teas />
          </Route>
      
          
        </Switch>
        <Footer />
    </Router>
    </div>
  );}
}

function Home(){
  document.title = "Yuusuke"
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <Games />
        <div className="backgroundContent py-5">
          <div className="content container">
            <TwitchPlayer />
            <Module game="guildwars2" position={1} />
            <Module game="battlefield5" position={0} />
            <Module game="battlefield1" position={1} />
            <Module game="csgo" position={0} />
            <Module game="apex" position={1} />
          </div>
        </div>
      </div>

      
      </>
  )
}

function Projects(){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            <Project />
          </div>
        </div>
      </div>
    </>
  )
}

function Guilds(guild){
  return (
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            {displayGuild(guild)}
          </div>
        </div>
      </div>
    </>
  )
}

function Skyrim(){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            <Mods game="Skyrim" />
          </div>
        </div>
      </div>
    </>
  )
}

function Fallout4(){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            <Mods game="Fallout4" />
          </div>
        </div>
      </div>
    </>
  )
}

function Raid(){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            <Raids />
          </div>
        </div>
      </div>
    </>
  )
}

function Records(team){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            {Record(team)}
          </div>
        </div>
      </div>
    </>
  )
}

function Logs(team){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
        <div className="backgroundContent py-5">
          <div className="content container">
            {Log(team)}
          </div>
        </div>
      </div>
    </>
  )
}

function Teas(){
  return(
    <> {window.scrollTo(0, 0)}
      <div className="page">
            <Tea />
      </div>
    </>
  )
}

