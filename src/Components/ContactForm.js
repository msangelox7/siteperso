// Make sure to run npm install @formspree/react
// For more help visit https://formspr.ee/react-help

import React from 'react';
import { useForm, ValidationError } from '@formspree/react';
import './ContactForm.css'

export default function ContactForm() {
  const [state, handleSubmit] = useForm("xayaywpe");
  if (state.succeeded) {
      return <p>Mail envoyé !</p>;
  }
  return (
      <form className="formulaire" onSubmit={handleSubmit}>
      <label className="textForm" htmlFor="email">
        Email
      </label>
      <input
        id="email"
        type="email" 
        name="email"
        placeholder="exemple@mail.com"
      />
      <ValidationError 
        prefix="Email" 
        field="email"
        errors={state.errors}
      />
      <label className="textForm">
        Message
      </label>
      <textarea
        id="message"
        name="message"
        placeholder="Votre message ici ..."
        rows="5"
      />
      <ValidationError 
        prefix="Message" 
        field="message"
        errors={state.errors}
      />
      <button type="submit" disabled={state.submitting}>
        Envoyer
      </button>
    </form>
  );
}