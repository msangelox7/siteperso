import React from "react";
import "./Footer.css";

function Footer() {
    return (
      <footer className="footer">
        ©2018-2021 Yuusuke - Vincent Allio
      </footer>
    )

}

export default Footer;
