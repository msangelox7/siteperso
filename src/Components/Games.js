import React, {Component} from "react";
import Carousel from "react-bootstrap/Carousel";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Games.css";
import gw2 from "../Pictures/guildwars2.jpg";
import battlefield from "../Pictures/battlefield5.jpg";
import csgo from "../Pictures/csgo.jpg";
import apex from "../Pictures/apex.jpg";
import bf2042 from "../Pictures/battlefield2042.jpg";

export class Games extends Component {
  render() {
    return (
      <Carousel>
        <Carousel.Item className="slideItem">
          <div className="slideBlurbf2042"></div>
          <img
            className="imageSlide d-block w-100"
            src={bf2042}
            alt="First slide"
          />
          <Carousel.Caption>
            <h3>Battlefield 2042</h3>
            <p>Prochainement !</p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="slideItem">
          <div className="slideBlurGW2"></div>
          <img
            className="imageSlide d-block w-100"
            src={gw2}
            alt="Second slide"
          />
          <Carousel.Caption>
            <h3>Guild Wars 2</h3>
            <p>Raids / Fractales / Contenu PvE plus largement</p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="slideItem">
          <div className="slideBlurCS"></div>
          <img
            className="imageSlide d-block w-100"
            src={csgo}
            alt="Third slide"
          />
          <Carousel.Caption>
            <h3>Counter-Strike</h3>
            <p>Que ce soit 1.6, Source ou Global Offensive</p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="slideItem">
          <div className="slideBlurBF"></div>
          <img
            className="imageSlide d-block w-100"
            src={battlefield}
            alt="Fourth slide"
          />
          <Carousel.Caption>
            <h3>Battlefield</h3>
            <p>Principalement Battlefield 1 et Battlefield 5</p>
          </Carousel.Caption>
        </Carousel.Item>

        <Carousel.Item className="slideItem">
          <div className="slideBlurApex"></div>
          <img
            className="imageSlide d-block w-100"
            src={apex}
            alt="Fifth slide"
          />
          <Carousel.Caption>
            <h3>Apex Legends</h3>
            <p>Solo ranked sur certaines saisons</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    );
  }
}

export default Games;
