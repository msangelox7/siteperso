import React from 'react';
import {Link} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';
import './Guild.css';
import guildsData from './JSONdata/guilds.json';

function moduleGuild(name, tag, position) {
  return (
    <>
      <Link to={`/guilds/${name}`} className="col-md-6">
        <div className="row justify-content-evenly align-items-center">
          {position == 1 ? (
            <>
              <div className="col-md-4 dividerGuild">
                <img
                  className="imageGuild"
                  src={`./img/logo${tag}.png`}
                  alt={`Module Logo ${name}`}
                />
              </div>
              <div className="col-md-7 dividerGuild">
                <div className="guildName">
                  <p>{`${name} [${tag}]`}</p>
                </div>
              </div>
              <div className="col-md-1 dividerGuild middleDivider" />
            </>
          ) : !position ? (
            <>
              <div className="col-md-7 offset-md-1 dividerGuild">
                <div className="guildName">
                  <p>{`${name} [${tag}]`}</p>
                </div>
              </div>
              <div className="col-md-4 dividerGuild">
                <img
                  className="imageGuild"
                  src={`./img/logo${tag}.png`}
                  alt={`Module Logo ${name}`}
                />
              </div>
            </>
          ) : (
            <>
              <div className="col-md-4">
                <img
                  className="imageGuild"
                  src={`./img/logo${tag}.png`}
                  alt={`Module Logo ${name}`}
                />
              </div>
              <div className="col-md-7">
                <div className="guildName">
                  <p>{`${name} [${tag}]`}</p>
                </div>
              </div>
            </>
          )}
        </div>
      </Link>
    </>
  );
}

function cardGuild(data){
  document.title = "Yuusuke - Guilds"
  return(
    <>
      <div className="imgGuild col-md-4">
        <img src={`/img/logo${data.tag}.png`} alt={`Logo ${data.name}`} />
      </div>
      <div className="col-md-8">
        <div className="guildHeader row">
          <div className="nameGuild col-md-12">
            <h2>{data.name} [{data.tag}]</h2>
          </div>
          <div className="levelGuild col-md-5 offset-md-1">
            <p>Level : {data.level}</p>
          </div>
          <div className="orientationGuild col-md-5">
            <p>Orientation : {data.orientation}</p>
          </div>
        </div>

        <div className="leadersGuild row">
          <div className="GMsGuild col-md-4 offset-md-1">
            <h4>Maîtres de guilde</h4>
            {
              data.guildMaster.map((player, index) =>(
                <p key={index}>{player}</p>
              ))
            }
          </div>
          <div className="offisGuild col-md-4 offset-md-2">
            {data.officer.length >= 1 &&
              <h4>Officiers de guilde</h4>
            }
            {
              data.officer.map((player, index) =>(
                <p key={index}>{player}</p>
              ))
            }
          </div>
        </div>
      </div>
    </>
  )
}

function displayGuildwars2(data, displayGuilds, guildName) {
  return (
    <>
      {displayGuilds == 'module' ? (
          <div className="moduleGuild row justify-content-evenly align-items-center">
            {moduleGuild(data[0].name, data[0].tag, 1)}
            {moduleGuild(data[1].name, data[1].tag, 0)}
            {moduleGuild(data[2].name, data[2].tag, 1)}
            {moduleGuild(data[3].name, data[3].tag, 0)}
          </div>
      ):(<>
          {
            data.map((guild, index) =>(
              <>
              {guild.name === guildName &&
                <div className="cardGuild row align-items-center" key={index}>
                {cardGuild(guild)}
                </div>
              }
              </>
            ))
          }
      </>)}
    </>
  );
}

export default function Guild({ game, displayGuilds }) {
  /* Getting Json Data */
  const guildsList = guildsData.guilds;
  /* ----------------- */

  return game == 'guildwars2'
    ? displayGuildwars2(guildsList, displayGuilds)
    : game == 'csgo' && (
    <>
      <>
        <a href="#">
          <div>
            <div className="txtTeam">
              <p>{`${guildsData.csgo.team}`}</p>
            </div>
          </div>
          <div className="imageTeam">
            <img
              src={`./img/logo${guildsData.csgo.team}.png`}
              alt={`Module Logo ${guildsData.csgo.team}`}
            />
          </div>
        </a>
      </>
    </>
    );
}

export function displayGuild({guild}){
  const guildsList = guildsData.guilds;
  switch (guild){
    case "kalimero":
      return(
        <div className="guildContainer">
          {displayGuildwars2(guildsList, "card", "Kalimero")}
        </div>
      );
    case "therainbowtriad":
      return(
        <div className="guildContainer">
        {displayGuildwars2(guildsList, "card", "The Rainbow Triad")}
        </div>
      );
    case "raidiliketofarm":
      return(
        <div className="guildContainer">
        {displayGuildwars2(guildsList, "card", "Raid I Like To Farm")}
        </div>
      );
    case "neosynergy":
      return(
        <div className="guildContainer">
        {displayGuildwars2(guildsList, "card", "Neo Synergy")}
        </div>
      );
    default:
      return(
        <>
        {displayGuild({guild: "kalimero"})}
        {displayGuild({guild: "therainbowtriad"})}
        {displayGuild({guild: "raidiliketofarm"})}
        {displayGuild({guild: "neosynergy"})}
        </>
      );
  }
}
