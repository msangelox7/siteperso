import React from 'react';
import './Loading.css';
import ReactLoading from 'react-loading';

export default function Loading(){
    return(
        <>
        <ReactLoading type='spinningBubbles' color="#fff" height={250} width={250} />
        <div className="loadingText">Chargement en cours ...</div>
        </>
    )
}