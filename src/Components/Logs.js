import React from 'react';
import {useLocation} from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import './Logs.css';
import dataLogs from './JSONdata/logs.json';

export default function Logs(source){
    const search = useLocation().search;
    let log = new URLSearchParams(search).get('log');
    let nameTeam, tagTeam
    if (source.team == "WIPE"){
        nameTeam = "Stay Focus And We Never"
        tagTeam = "WIPE"
    }
    else if(source.team == "KALI"){
        nameTeam = "Kalimero"
        tagTeam = "WIPE"
    }
    document.title = "Yuusuke - " + tagTeam + " logs"

    const displayLogs = (event) =>{
        location.href = "/" + tagTeam + "/logs/?log=" + event.target.value;
    }
    const weekList = dataLogs[tagTeam]
    console.log(weekList)
    let logList = []
    weekList.map(week => (week.name == log && (logList=week.data)))
    console.log(logList);
    return(
        <>
            <div className="pageHeader">
                <h1>Logs {nameTeam}<br/>[{tagTeam}]</h1>
            </div>

            <Form.Group>
                <Form.Control size="md" as="select" onChange={displayLogs}>
                    {log == null ? (<option>Selectionnez une semaine ...</option>):(<option>{log}</option>)}
                    {weekList.map((week, index) => (week.name != log && <option key={index}>{week.name}</option>))}
                </Form.Control>
            </Form.Group>

            {log != null && <div className="logTable">
                {logList.map((log, index) => {
                    switch(log.bossName){
                        case "Gardien de la Vallée":
                                return(
                                <>
                                    <div className="dividerLog"></div>
                                    <h3>Aile 1</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        case "Paressor":
                                return(
                                <>
                                <div className="dividerLog"></div>
                                    <h3>Aile 2</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        case "Titan du Fort":
                                return(
                                <>
                                <div className="dividerLog"></div>
                                    <h3>Aile 3</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        case "Cairn":
                                return(
                                <>
                                <div className="dividerLog"></div>
                                    <h3>Aile 4</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        case "Horreur sans Âme":
                                return(
                                <>
                                <div className="dividerLog"></div>
                                    <h3>Aile 5</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        case "Amalgame Conjuré":
                                return(
                                <>
                                <div className="dividerLog"></div>
                                    <h3>Aile 6</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        case "Cardinal Sabir":
                                return(
                                <>
                                <div className="dividerLog"></div>
                                    <h3>Aile 7</h3>
                                    <div className="bossLog" key={index}>
                                        <a href={log.log}>
                                            <p className="bossNameLog">{log.bossName}</p>
                                            <p className="bossLinkLog">{log.log}</p>
                                        </a>
                                    </div>
                                </>
                                );
                        default:
                            return(
                                <div className="bossLog" key={index}>
                                    <a href={log.log}>
                                        <p className="bossNameLog">{log.bossName}</p>
                                        <p className="bossLinkLog">{log.log}</p>
                                    </a>
                                </div>
                            );
                            
                    }
                })}
            </div>}
        </>
    )
}

