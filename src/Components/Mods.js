import React from 'react';
import Table from 'react-bootstrap/Table';
import PropTypes from "prop-types";
import './Mods.css';
import modsSkyrim from'./JSONdata/modsSkyrim.json';
import modsFallout4 from './JSONdata/modsFallout4.json';

export default function Mods({game}){
    document.title = "Yuusuke - " + game + " mods"
    return(
        <>
        <div className="pageHeader">
        <h1>Liste des mods sur {game}</h1>
        <p>Nombre total de mods : {
            (game === "Skyrim" ? (
                modsSkyrim.length
            ):(game === "Fallout4" &&
                modsFallout4.length
            ))
        }</p>
        </div>
        <Table striped bordered hover responsive variant="dark" size="sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nom du mod</th>
                    <th>Catégorie</th>
                    <th>Version</th>
                </tr>
            </thead>
            <tbody>
                {
                    (game === "Skyrim" ? (
                        modsSkyrim.map((mod, index) => (
                            lineMod(mod, index)
                        ))
                    ):(game === "Fallout4" &&
                        modsFallout4.map((mod, index) => (
                            lineMod(mod, index)
                    ))
                    ))
                    
                }
            </tbody>
        </Table>
        </>
    )
}
Mods.propTypes = {
    game: PropTypes.string.isRequired
  };

function lineMod(data, index){
    return(
        <tr>
            <td>{index+1}</td>
            <td><a href={data.link}>{data.name}</a></td>
            <td>{data.categorie}</td>
            <td>{data.version}</td>
        </tr>
    )
}