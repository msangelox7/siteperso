import React from "react";
import {Link} from "react-router-dom";
import "./Module.css";
import PropTypes from "prop-types";
import {txtGame, titleGame} from "./TextGames";

const Module = ({game, position}) => (
  <>
    <div className="globalModule">
      <div className="module row justify-content-evenly align-items-center">
        {position ? (
          <>
            <div className="col-lg-6 tab">
              <img
                className="imageModule"
                src={`../img/${game}Module.jpg`}
                alt={`Module ${game}`}
              />
            </div>
            <div className="col-lg-6 tab">
              <div className="description">
              {game === "guildwars2" ? (
                  <Link to="/guilds">
                  <h3>{titleGame(game)}</h3>
                  </Link>
                ):(
                  
                  <h3>{titleGame(game)}</h3>
                )}
                <div>{txtGame(game)}</div>
              </div>
            </div>
          </>
        ) : (
          <>
            <div className="col-lg-6 tab">
              <div className="description">
                {game === "guildwars2" ? (
                  <Link to="/guilds">
                  <h3>{titleGame(game)}</h3>
                  </Link>
                ):(
                  
                  <h3>{titleGame(game)}</h3>
                )}
                
                <div>{txtGame(game)}</div>
              </div>
            </div>
            <div className="col-lg-6 tab">
              <img
                className="imageModule "
                src={`../img/${game}Module.jpg`}
                alt={`Module ${game}`}
              />
            </div>
          </>
        )}
      </div>
      <div className="divider"></div>
    </div>
  </>
);

Module.propTypes = {
  game: PropTypes.string.isRequired,
  position: PropTypes.number.isRequired
};

export default Module;
