import React from 'react';
import listProjects from "./JSONdata/projects.json"
import ContactForm from './ContactForm';
import "./Project.css";

export default function Project(){
    document.title = "Yuusuke - Projets"
    return(
        <>
        <div className="pageHeader">
            <h1>Liste des projets</h1>
        </div>
        <div className="projectList">
        {listProjects.projects.map((project) => (
            <div className="projectContainer" key={project.name}>
                {displayProject(project)}
            </div>
        ))}
        </div>
        <div id="contact">
        <h1>Contact</h1>
        <p>
            Pour toute réclamation ou demande, veuillez utiliser ce formulaire de contact,<br/> ou en envoyer un email à cette adresse :&nbsp;
            <a href="mailto:contact.yuusuke@gmail.com">contact.yuusuke@gmail.com</a>
        </p>
            <ContactForm />
        </div>
        </>
    )
}

function displayProject(project){
    return(
        <>
            <div className="imgContainer">
                <a href={project.websiteLink} target="_blank" rel="noopener noreferrer">
                    <img src={`./img/projects_preview/${project.name}.jpeg`} alt={`${project.name} preview`} />
                    <div className="nameProject">
                        <p>{project.name}</p>
                    </div>
                </a>
            </div>
            <div className="websiteLink">
                <a href={project.gitlabLink} target="_blank" rel="noopener noreferrer"><p>Lien vers le Git</p></a>
            </div>
            <div className="technologiesProject">
                {
                    project.technologies.map((techno) =>(
                        <div className={`${techno}Container`} key={techno}>
                            <p>{techno}</p>
                        </div>
                    ))
                }
            </div>  
        </>
    )
}