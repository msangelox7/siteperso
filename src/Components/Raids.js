import React, { useState, useEffect } from 'react';
import {useLocation} from 'react-router-dom';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'
import './Raids.css';
import languageData from './JSONdata/languages.json';

export default function Raids(){
    const search = useLocation().search;
    let api = new URLSearchParams(search).get('api'); // Get Query data "api"
    

    const [JSONData, setJSONData] = useState(null);
    const [AccountData, setAccountData] = useState(null);
    
    const bossList = [
        "vale_guardian","spirit_woods","gorseval","sabetha",
        "slothasor","bandit_trio","matthias",
        "escort","keep_construct","twisted_castle","xera",
        "cairn","mursaat_overseer","samarog","deimos",
        "soulless_horror","river_of_souls","statues_of_grenth","voice_in_the_void",
        "conjured_amalgamate","twin_largos","qadim",
        "gate","sabir","adina","qadim_the_peerless"
    ]

    //const myApi = "6E6CC2B1-48B8-6844-B5A7-17A82367DF1F81B51195-7274-4014-B555-DF929E9C025D"

  useEffect(() => {
    checkLocal()
    api != null && 
    fetch("https://api.guildwars2.com/v2/account/raids?access_token=" + api)
      .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
          return response.json();
        } else {
          throw Error(response.status);
        }
      })
      .then((data) => setJSONData(data)).catch(() => {
        // Handle the error
        alert("Error 401 ! Clé API incorrecte ! Veuillez entrer une clé valide.")
      });
      api != null && 
      fetch("https://api.guildwars2.com/v2/account?access_token=" + api)
      .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
          return response.json();
        } else {
          throw Error(response.status);
        }
      })
      .then((data) => setAccountData(data)).catch(() => {
        // Handle the error
        alert("Error 401 ! Clé API incorrecte ! Veuillez entrer une clé valide.")
      });
  }, []);

  const [key, setKey] = useState(" ");
  const handleInput = event => {
    setKey(event.target.value);
  };
  var localStorageAPIKey;
  const apiKeySave = localStorage.getItem(localStorageAPIKey)
  console.log(apiKeySave)

  const checkLocal = () => {
    if(apiKeySave){
      api = apiKeySave
    }
  }

  const logValue = () => {
    fetch("https://api.guildwars2.com/v2/account/raids?access_token=" + key)
      .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
          localStorage.setItem(localStorageAPIKey, key);
          location.href = "/guildwars2?api=" + key;
        } else {
          throw Error(response.status);
        }
      })
      .then((data) => setJSONData(data)).catch(() => {
        // Handle the error
        alert("Error 401 ! Clé API incorrecte ! Veuillez entrer une clé valide.")
      });
  };

  const [language, setLanguage] = useState("en");
  let languageText

  switch(language){
    case "fr":
      languageText = languageData.fr;
      break;
    case "en":
      languageText = languageData.en;
      break;
    case "de":
      languageText = languageData.de;
      break;
  }
  if(AccountData != null){
    document.title = "Yuusuke - " + AccountData.name
  }

  return (
    <>
        <div className={`pageHeader ${checkWing(bossList, JSONData)}`}>
            <h1>{languageText.weekly_raid_bosses}</h1>
            <img className="imageClear" src="./img/star.svg" />
        </div>

        <div className="apiForm">
        <Form.Label htmlFor="inputPassword5">{languageText.key} API</Form.Label>
          <Form.Control
          type="password"
          id="inputPassword5"
          aria-describedby="passwordHelpBlock"
          onChange={handleInput}
        />
        <Form.Text id="passwordHelpBlock" muted>
          {languageText.key_helper}
        </Form.Text>
          <Button className="buttonAPI"
          onClick={logValue}>{languageText.send}</Button>
        </div>
        <div className="divider" />
        <h1 className="nameAccount">{AccountData != null && AccountData.name}</h1>

      <div className="bossTable row justify-content-evenly">
          <div className={`wing col-md-4 ${checkWing([bossList[0],bossList[1],bossList[2],bossList[3]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 1</h2>
              <div className={`boss ${checkBoss(bossList[0], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/vale_guardian.jpg" alt="vale_guardian" />
                <p className="nameBoss">{languageText.vale_guardian}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[1], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/spirit_woods.jpg" alt="spirit_woods" />
                <p className="nameBoss">{languageText.spirit_wood}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[2], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/gorseval.jpg" alt="gorseval" />
                <p className="nameBoss">{languageText.gorseval}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[3], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/sabetha.jpg" alt="sabetha" />
                <p className="nameBoss">{languageText.sabetha}</p>
              </div>
          </div>
          <div className={`wing col-md-4 ${checkWing([bossList[4],bossList[5],bossList[6]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 2</h2>
              <div className={`boss ${checkBoss(bossList[4], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/slothasor.jpg" alt="slothasor" />
                <p className="nameBoss">{languageText.slothasor}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[5], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/trio.jpg" alt="trio" />
                <p className="nameBoss">{languageText.bandit_trio}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[6], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/matthias.jpg" alt="matthias" />
                <p className="nameBoss">{languageText.matthias}</p>
              </div>
          </div>
          <div className={`wing col-md-4 ${checkWing([bossList[7],bossList[8],bossList[9],bossList[10]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 3</h2>
              <div className={`boss ${checkBoss(bossList[7], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/escort.jpg" alt="escort" />
                <p className="nameBoss">{languageText.escort}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[8], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/keep_construct.jpg" alt="keep_construct" />
                <p className="nameBoss">{languageText.keep_construct}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[9], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/twisted_castle.jpg" alt="twisted_castle" />
                <p className="nameBoss">{languageText.twisted_castle}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[10], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/xera.jpg" alt="xera" />
                <p className="nameBoss">{languageText.xera}</p>
              </div>
          </div>

          <div className={`wing col-md-4 ${checkWing([bossList[11],bossList[12],bossList[13],bossList[14]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 4</h2>
              <div className={`boss ${checkBoss(bossList[11], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/cairn.jpg" alt="cairn" />
                <p className="nameBoss">{languageText.cairn}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[12], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/mursaat_overseer.jpg" alt="mo" />
                <p className="nameBoss">{languageText.mursaat_overseer}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[13], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/samarog.jpg" alt="samarog" />
                <p className="nameBoss">{languageText.samarog}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[14], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/deimos.jpg" alt="deimos" />
                <p className="nameBoss">{languageText.deimos}</p>
              </div>
          </div>
          <div className={`wing col-md-4 ${checkWing([bossList[15],bossList[16],bossList[17],bossList[18]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 5</h2>
              <div className={`boss ${checkBoss(bossList[15], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/soulless_horror.jpg" alt="soulless_horror" />
                <p className="nameBoss">{languageText.soulless_horror}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[16], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/river.jpg" alt="river" />
                <p className="nameBoss">{languageText.river_of_souls}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[17], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/statues.jpg" alt="statues" />
                <p className="nameBoss">{languageText.statues_of_grenth}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[18], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/dhuum.jpg" alt="dhuum" />
                <p className="nameBoss">{languageText.dhuum}</p>
              </div>
          </div>
          <div className={`wing col-md-4 ${checkWing([bossList[19],bossList[20],bossList[21]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 6</h2>
              <div className={`boss ${checkBoss(bossList[19], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/conjured_amalgamate.jpg" alt="conjured_amalgamate" />
                <p className="nameBoss">{languageText.conjured_amalgamate}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[20], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/largos.jpg" alt="largos" />
                <p className="nameBoss">{languageText.twin_largos}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[21], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/qadim.jpg" alt="qadim" />
                <p className="nameBoss">{languageText.qadim}</p>
              </div>
          </div>

          <div className={`wing col-md-4 ${checkWing([bossList[22],bossList[23],bossList[24],bossList[25]], JSONData)}`}>
              <img className="imageCheck" src="./img/republic.svg" />
              <h2>{languageText.wing} 7</h2>
              <div className={`boss ${checkBoss(bossList[22], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/gate2.jpg" alt="gate2" />
                <p className="nameBoss">{languageText.gate}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[23], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/sabir.jpg" alt="sabir" />
                <p className="nameBoss">{languageText.sabir}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[24], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/adina.jpg" alt="adina" />
                <p className="nameBoss">{languageText.adina}</p>
              </div>
              <div className={`boss ${checkBoss(bossList[25], JSONData)}`}>
                <img className="checkBoss" src="./img/done.svg" />
                <img className="uncheckBoss" src=".\img\undone.svg" />
                <img className="imageBoss" src="./img/raidBosses/bosses/qadim_the_peerless.jpg" alt="qadim_the_peerless" />
                <p className="nameBoss">{languageText.qadim_the_peerless}</p>
              </div>
          </div>
      </div>
      <div className="languages">
        <img className="flag" src="../img/flags/fr.svg" alt="fr_flag" onClick={() => setLanguage("fr")}/>
        <img className="flag" src="../img/flags/en.svg" alt="en_flag" onClick={() => setLanguage("en")}/>
        <img className="flag" src="../img/flags/de.svg" alt="en_flag" onClick={() => setLanguage("de")}/>
      </div>
    </>
  )
}

function checkBoss(boss, JSONData){
    if(JSONData != null){
        for(let i = 0; i < JSONData.length; i++){
            if(boss === JSONData[i]){
                return "checked"
            }       
        }
    }
    return ""
}

function checkWing(bosses, JSONData){
    if(JSONData != null){
    for(let i = 0; i < bosses.length; i++){
        let notDone = true
        for(let j = 0; j < JSONData.length; j++){
            if(bosses[i] === JSONData[j]){
                notDone = false
            }
        }
        if (notDone){
            return ""
        }
    }
    return "checked"
    }
    return ""
}