import React, {useState} from 'react';
import './Record.css';
import data from './JSONdata/record.json';
import languageData from './JSONdata/languages.json';

export default function Record(source){
    const [language, setLanguage] = useState("en");
    let languageText
    let recordsTable
    let nameTeam, tagTeam
    if (source.team == "WIPE"){
        recordsTable = data.WIPE
        nameTeam = "Stay Focus And We Never"
        tagTeam = "WIPE"
    }
    else if(source.team == "KALI"){
        recordsTable = data.KALI
        nameTeam = "Kalimero"
        tagTeam = "KALI"
    }
    document.title = "Yuusuke - Records - " + tagTeam

    switch(language){
      case "fr":
        languageText = languageData.fr;
        break;
      case "en":
        languageText = languageData.en;
        break;
      case "de":
        languageText = languageData.de;
        break;
    }


    console.log(languageText.vale_guardian);
    

    return(
        <>
        <div className="pageHeader">
            <img className="imageLogoL" src={`../img/logo${tagTeam}.png`} />
            <h1>{languageText.record} {nameTeam}<br/>[{tagTeam}]</h1>
            <img className="imageLogoR" src={`../img/logo${tagTeam}.png`} />
        </div>

            <div className="bossTable row justify-content-evenly">
          <div className="wing col-md-4">
              <h2>{languageText.wing} 1</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/vale_guardian.jpg" alt="vale_guardian" />
                <p className="nameBoss">{languageText.vale_guardian}</p>
                <div className="record">
                    <p className="date">{recordsTable[0].date}</p>
                    <p className="time">{recordsTable[0].time}</p>
                    <a href={`${recordsTable[0].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/gorseval.jpg" alt="gorseval" />
                <p className="nameBoss">{languageText.gorseval}</p>
                <div className="record">
                    <p className="date">{recordsTable[1].date}</p>
                    <p className="time">{recordsTable[1].time}</p>
                    <a href={`${recordsTable[1].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/sabetha.jpg" alt="sabetha" />
                <p className="nameBoss">{languageText.sabetha}</p>
                <div className="record">
                    <p className="date">{recordsTable[2].date}</p>
                    <p className="time">{recordsTable[2].time}</p>
                    <a href={`${recordsTable[2].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>
          <div className="wing col-md-4">
              <h2>{languageText.wing} 2</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/slothasor.jpg" alt="slothasor" />
                <p className="nameBoss">{languageText.slothasor}</p>
                <div className="record">
                    <p className="date">{recordsTable[3].date}</p>
                    <p className="time">{recordsTable[3].time}</p>
                    <a href={`${recordsTable[3].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/matthias.jpg" alt="matthias" />
                <p className="nameBoss">{languageText.matthias}</p>
                <div className="record">
                    <p className="date">{recordsTable[4].date}</p>
                    <p className="time">{recordsTable[4].time}</p>
                    <a href={`${recordsTable[4].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>
          <div className="wing col-md-4">
              <h2>{languageText.wing} 3</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/keep_construct.jpg" alt="keep_construct" />
                <p className="nameBoss">{languageText.keep_construct}</p>
                <div className="record">
                    <p className="date">{recordsTable[5].date}</p>
                    <p className="time">{recordsTable[5].time}</p>
                    <a href={`${recordsTable[5].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/xera.jpg" alt="xera" />
                <p className="nameBoss">{languageText.xera}</p>
                <div className="record">
                    <p className="date">{recordsTable[6].date}</p>
                    <p className="time">{recordsTable[6].time}</p>
                    <a href={`${recordsTable[6].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>

          <div className="wing col-md-4">
              <h2>{languageText.wing} 4</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/cairn.jpg" alt="cairn" />
                <p className="nameBoss">{languageText.cairn}</p>
                <div className="record">
                    <p className="date">{recordsTable[7].date}</p>
                    <p className="time">{recordsTable[7].time}</p>
                    <a href={`${recordsTable[7].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/mursaat_overseer.jpg" alt="mo" />
                <p className="nameBoss">{languageText.mursaat_overseer}</p>
                <div className="record">
                    <p className="date">{recordsTable[8].date}</p>
                    <p className="time">{recordsTable[8].time}</p>
                    <a href={`${recordsTable[8].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/samarog.jpg" alt="samarog" />
                <p className="nameBoss">{languageText.samarog}</p>
                <div className="record">
                    <p className="date">{recordsTable[9].date}</p>
                    <p className="time">{recordsTable[9].time}</p>
                    <a href={`${recordsTable[9].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/deimos.jpg" alt="deimos" />
                <p className="nameBoss">{languageText.deimos}</p>
                <div className="record">
                    <p className="date">{recordsTable[10].date}</p>
                    <p className="time">{recordsTable[10].time}</p>
                    <a href={`${recordsTable[10].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>
          <div className="wing col-md-4">
              <h2>{languageText.wing} 5</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/soulless_horror.jpg" alt="soulless_horror" />
                <p className="nameBoss">{languageText.soulless_horror}</p>
                <div className="record">
                    <p className="date">{recordsTable[11].date}</p>
                    <p className="time">{recordsTable[11].time}</p>
                    <a href={`${recordsTable[11].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/dhuum.jpg" alt="dhuum" />
                <p className="nameBoss">{languageText.dhuum}</p>
                <div className="record">
                    <p className="date">{recordsTable[12].date}</p>
                    <p className="time">{recordsTable[12].time}</p>
                    <a href={`${recordsTable[12].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>
          <div className="wing col-md-4">
              <h2>{languageText.wing} 6</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/conjured_amalgamate.jpg" alt="conjured_amalgamate" />
                <p className="nameBoss">{languageText.conjured_amalgamate}</p>
                <div className="record">
                    <p className="date">{recordsTable[13].date}</p>
                    <p className="time">{recordsTable[13].time}</p>
                    <a href={`${recordsTable[13].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/largos.jpg" alt="largos" />
                <p className="nameBoss">{languageText.twin_largos}</p>
                <div className="record">
                    <p className="date">{recordsTable[14].date}</p>
                    <p className="time">{recordsTable[14].time}</p>
                    <a href={`${recordsTable[14].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/qadim.jpg" alt="qadim" />
                <p className="nameBoss">{languageText.qadim}</p>
                <div className="record">
                    <p className="date">{recordsTable[15].date}</p>
                    <p className="time">{recordsTable[15].time}</p>
                    <a href={`${recordsTable[15].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>

          <div className="wing col-md-4">
              <h2>{languageText.wing} 7</h2>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/sabir.jpg" alt="sabir" />
                <p className="nameBoss">{languageText.sabir}</p>
                <div className="record">
                    <p className="date">{recordsTable[16].date}</p>
                    <p className="time">{recordsTable[16].time}</p>
                    <a href={`${recordsTable[16].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/adina.jpg" alt="adina" />
                <p className="nameBoss">{languageText.adina}</p>
                <div className="record">
                    <p className="date">{recordsTable[17].date}</p>
                    <p className="time">{recordsTable[17].time}</p>
                    <a href={`${recordsTable[17].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
              <div className="bossR">
                <img className="imageBossR" src="../img/raidBosses/bosses/qadim_the_peerless.jpg" alt="qadim_the_peerless" />
                <p className="nameBoss">{languageText.qadim_the_peerless}</p>
                <div className="record">
                    <p className="date">{recordsTable[18].date}</p>
                    <p className="time">{recordsTable[18].time}</p>
                    <a href={`${recordsTable[18].log}`} className="log" target="_blank" rel="noopener noreferrer">Log</a>
                </div>
              </div>
          </div>
      </div>
      <div className="languages">
        <img className="flag" src="../img/flags/fr.svg" alt="fr_flag" onClick={() => setLanguage("fr")}/>
        <img className="flag" src="../img/flags/en.svg" alt="en_flag" onClick={() => setLanguage("en")}/>
        <img className="flag" src="../img/flags/de.svg" alt="en_flag" onClick={() => setLanguage("de")}/>
      </div>
        </>
    )
}