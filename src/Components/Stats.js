import { useState, useEffect } from 'react';

export default function Stats() {
  const [JSONData, setJSONData] = useState(null);

  useEffect(() => {
    fetch('https://api.tracker.gg/api/v2/bfv/standard/profile/origin/Yuuusuuke?')
      .then((response) => response.json())
      .then((data) => setJSONData(data));
  }, []);

  return (
    console.log(JSONData)
  );
}
