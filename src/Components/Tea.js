/* eslint-disable no-unused-vars */
import React from "react";
import ReactDOM from "react-dom";
import './Tea.css';
import ListTea from './JSONdata/thé.json';
import reactDom from "react-dom";

var théNoir = false,
    théVert = false,
    infusion = false;

export default function Tea(){
    return(
        <>
            <div className="header">
                <h1 className="header__title">Liste des thés</h1>
            </div>
            
            <div className="backgroundContent py-5">
                <div className="content container">
                    <div className="filter">
                        <div className="filter__card">
                            <label htmlFor="thé noir" className="card__text">Thé noir</label>
                            <input type="checkbox" id="thé noir" name="thé noir" className="card__input"  onClick={(() => filters("thé noir"))} />
                        </div>
                        <div className="filter__card">
                            <label htmlFor="thé vert" className="card__text">Thé vert</label>
                            <input type="checkbox" id="thé vert" name="thé vert" className="card__input"  onClick={(() => filters("thé vert"))} />
                        </div>
                        <div className="filter__card">
                            <label htmlFor="infusion" className="card__text">Infusion</label>
                            <input type="checkbox" id="infusion" name="infusion" className="card__input"  onClick={(() => filters("infusion"))} />
                        </div>
                    </div>

                    <div className="table">
                    {ListTea.map((element, key) => (
                        displayTea(element, key)
                    ))}

                    <div className="filler"></div>
                    <div className="filler"></div>
                    </div>
                </div>
            </div>
            
        </>
    )
}

function displayTea(tea, key){
    return(
        <div className={`table__card card--${key}`} key={key}>
            <a href={tea.link}  target="_blank" rel="noopener noreferrer">
                <img src={tea.image} className="table__card__image" alt={tea.name}></img>
            </a>
            <div className="table__card__description">
                <p className="table__card__description__name">{tea.name}</p>
                <p className="table__card__description__price">{tea.price}</p>
            </div>
        </div>
    )
}

function eraseContent(parent){
    ReactDOM.unmountComponentAtNode(parent);
}

function filters(filter){
    var table = document.getElementsByClassName("table")[0];
    var render = <> </>;

    switch(filter){
        case "thé noir":
            théNoir = !théNoir;
            break;
        case "thé vert":
            théVert = !théVert;
            break;
        case "infusion":
            infusion = !infusion;
            break;
    }

    eraseContent(table);

    if(!théNoir && !théVert && !infusion){
        ListTea.map((element, key) =>(
            render = <> {render} {displayTea(element, key)} </>
        ))
    }
    else{
        ListTea.map((element, key) =>(
            element.tags.forEach(tag => {
                if(théNoir && tag == "thé noir"){
                    render = <> {render} {displayTea(element, key)} </>;
                }
                else if(théVert && tag == "thé vert"){
                    render = <> {render} {displayTea(element, key)} </>;
                }
                else if(infusion && tag == "infusion"){
                    render = <> {render} {displayTea(element, key)} </>;
                }
            })
        ))
    }

    render = <> {render} <div className="filler"></div><div className="filler"></div></>

    reactDom.render(render, table);
}