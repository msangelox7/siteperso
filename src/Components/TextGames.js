import React from 'react';
import Guild from './Guild';
import './TextGames.css';
import jsonData from './JSONdata/guilds.json';

export function txtGame(game) {
  switch (game) {
    case 'guildwars2':
      return <Guild game="guildwars2" displayGuilds="module" />;

    case 'battlefield2042':
      return (
        <div className="row justify-content-space-evenly">

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf2042/Sergeant_First_Class_Badge.png" />
            <div className="bfContent">
              <div className="bfTitle bf2042">Rank</div>
              <div className="bfScore">58</div>
              <div className="bfRatio bf2042">-----</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
          <div className="progressbf2042">
            <div className="active" style={{height: "82%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf2042">K/D</div>
              <div className="bfScore">1.89</div>
              <div className="bfRatio bf2042">Top 18%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
          <div className="progressbf2042">
            <div className="active" style={{height: "81%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf2042">Score/min</div>
              <div className="bfScore">427.31</div>
              <div className="bfRatio bf2042">Top 19%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
          <div className="progressbf2042">
            <div className="active" style={{height: "72%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf2042">Winrate</div>
              <div className="bfScore">56.0%</div>
              <div className="bfRatio bf2042">Top 28%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf2042/assault.png" />
            <div className="bfContent">
              <div className="bfTitle bf2042">K/D</div>
              <div className="bfScore">1.86</div>
              <div className="bfRatio bf2042">Top 15%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf2042/recon.png" />
            <div className="bfContent">
              <div className="bfTitle bf2042">K/D</div>
              <div className="bfScore">2.08</div>
              <div className="bfRatio bf2042">Top 13%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf2042/medic.png" />
            <div className="bfContent">
              <div className="bfTitle bf2042">K/D</div>
              <div className="bfScore">1.44</div>
              <div className="bfRatio bf2042">Top 25%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf2042/support.png" />
            <div className="bfContent">
              <div className="bfTitle bf2042">K/D</div>
              <div className="bfScore">1.90</div>
              <div className="bfRatio bf2042">Top 16%</div>
            </div></a>
          </div>
        
        </div>
      );
    case 'battlefield5':
      return (
        <div className="row justify-content-space-evenly">

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf5/Sergeant_First_Class_Badge.png" />
            <div className="bfContent">
              <div className="bfTitle bf5">Rank</div>
              <div className="bfScore">58</div>
              <div className="bfRatio bf5">-----</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
          <div className="progressbf5">
            <div className="active" style={{height: "82%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf5">K/D</div>
              <div className="bfScore">1.89</div>
              <div className="bfRatio bf5">Top 18%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
          <div className="progressbf5">
            <div className="active" style={{height: "81%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf5">Score/min</div>
              <div className="bfScore">427.31</div>
              <div className="bfRatio bf5">Top 19%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
          <div className="progressbf5">
            <div className="active" style={{height: "72%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf5">Winrate</div>
              <div className="bfScore">56.0%</div>
              <div className="bfRatio bf5">Top 28%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf5/assault.png" />
            <div className="bfContent">
              <div className="bfTitle bf5">K/D</div>
              <div className="bfScore">1.86</div>
              <div className="bfRatio bf5">Top 15%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf5/recon.png" />
            <div className="bfContent">
              <div className="bfTitle bf5">K/D</div>
              <div className="bfScore">2.08</div>
              <div className="bfRatio bf5">Top 13%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf5/medic.png" />
            <div className="bfContent">
              <div className="bfTitle bf5">K/D</div>
              <div className="bfScore">1.44</div>
              <div className="bfRatio bf5">Top 25%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bfv/profile/origin/Yuuusuuke/overview" target="_blank" rel="noopener noreferrer">
            <img className="logobf" src="../img/bf5/support.png" />
            <div className="bfContent">
              <div className="bfTitle bf5">K/D</div>
              <div className="bfScore">1.90</div>
              <div className="bfRatio bf5">Top 16%</div>
            </div></a>
          </div>
        
        </div>
      );
    case 'battlefield1':
      return (
        <div className="row justify-content-space-evenly">

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
            <img className="logobf1" src="../img/bf1/84.png" />
            <div className="bfContent">
              <div className="bfTitle bf1">Rank</div>
              <div className="bfScore">84</div>
              <div className="bfRatio bf1">-----</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
          <div className="progressbf1">
            <div className="active" style={{height: "95%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf1">K/D</div>
              <div className="bfScore">2.38</div>
              <div className="bfRatio bf1">Top 5%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
          <div className="progressbf1">
            <div className="active" style={{height: "90%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf1">Score/min</div>
              <div className="bfScore">1891</div>
              <div className="bfRatio bf1">Top 10%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
          <div className="progressbf1">
            <div className="active" style={{height: "66%"}}></div>
          </div>
            <div className="bfContent">
              <div className="bfTitle bf1">Winrate</div>
              <div className="bfScore">51.6%</div>
              <div className="bfRatio bf1">Top 34%</div>
            </div></a>
          </div>

          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
            <img className="logobf1" src="../img/bf1/assault.png" />
            <div className="bfContent">
              <div className="bfTitle bf1">KpM</div>
              <div className="bfScore">1.86</div>
              <div className="bfRatio bf1">Top 4%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
            <img className="logobf1" src="../img/bf1/recon.png" />
            <div className="bfContent">
              <div className="bfTitle bf1">KpM</div>
              <div className="bfScore">1.27</div>
              <div className="bfRatio bf1">Top 13%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
            <img className="logobf1" src="../img/bf1/medic.png" />
            <div className="bfContent">
              <div className="bfTitle bf1">KpM</div>
              <div className="bfScore">1.67</div>
              <div className="bfRatio bf1">Top 25%</div>
            </div></a>
          </div>
          <div className="bfCard col-md-3 col-6"><a href="https://battlefieldtracker.com/bf1/profile/pc/Yuuusuuke" target="_blank" rel="noopener noreferrer">
            <img className="logobf1" src="../img/bf1/support.png" />
            <div className="bfContent">
              <div className="bfTitle bf1">KpM</div>
              <div className="bfScore">1.58</div>
              <div className="bfRatio bf1">Top 16%</div>
            </div></a>
          </div>
        
        </div>
      );
    case 'csgo':
      return (
        <>
          <>
              <div className="csgoContent row justify-content-space-evenly">
                <div className="col-md-6">
                  <div className="csgoTable">
                    <Guild game="csgo" displayGuilds="table" />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="csgoTable">{csgoRank("MM", jsonData.csgo.rankMM)}</div>
                </div>
                <div className="col-md-6">
                  <div className="csgoTable">
                    {csgoRank("Wingman", jsonData.csgo.rankWingman)}
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="csgoTable">
                    {csgoRank("DangerZone", jsonData.csgo.rankDangerZone)}
                  </div>
                </div>
              </div>
            </>
        </>
      );

    case 'apex':
      return (
        <>
          <>
              <div className="apexContent row justify-content-space-evenly">
                <div className="col-md-6">
                  <div className="apexTable">
                  <p className="txtRank">Main Legend :</p>
                    <img
                      src={`./img/apex/logo${jsonData.apex.mainLegend}.png`}
                      alt={`Logo ${jsonData.apex.mainLegend}`}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="apexTable">
                  <p className="txtRank">Rank SoloQ :</p>
                    <img
                      src={`./img/apex/ranks/${jsonData.apex.rank}.webp`}
                      alt={`Logo ${jsonData.apex.rank}`}
                    />
                  </div>
                </div>
              </div>
            </>
        </>
      );
  }
}

function csgoRank(mod, rank) {
  return (
    <>
      <>
          <p className="txtRank">Rank {mod} :</p>
          <div className="imgRank">
            <img
              src={`./img/csgoRank/rank${mod}/csgo-rank${mod}-${rank}.webp`}
            />
          </div>
        </>
    </>
  );
}

export function titleGame(game) {
  switch (game) {
    case 'guildwars2':
      return 'Guild Wars 2';
    case 'battlefield5':
      return 'Battlefield V';
    case 'battlefield1':
      return 'Battlefield 1';
    case 'csgo':
      return 'Counter-Strike: Global Offensive';
    case 'apex':
      return 'Apex Legends';
    case 'battlefield2042':
      return "Battlefield 2042";
  }
}
