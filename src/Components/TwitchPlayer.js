import React from 'react';
import './TwitchPlayer.css';
import {TwitchEmbed} from 'react-twitch-embed';

export default function TwitchPlayer(){
    return (
        <div className="stream">
            <div className="playerTwitch">
                <TwitchEmbed
                    channel="Yuusuuke"
                    theme="dark"
                />
            </div>
            <div className="divider"></div>
        </div>
      );
}